const express = require("express");

const port = 5050;
const landingpageRoute = require("./controllers/LandingPage");
const gameRoute = require("./controllers/Game");
const formloginRoute = require("./controllers/FormLogin");
const loginRoute = require("./controllers/Login");
const apiRouter = require("./routes/api");
const formregisterRoute = require("./controllers/FormRegister");
const usersRoute = require("./controllers/Users");

const logger = (req, res, next) => {
    console.log(`${req.method} ${req.url}`);
    next();
};

const app = express();

app.use(express.static(__dirname + "/public"));
app.set("view engine", "ejs");
app.use(logger);
app.use(express.json());
app.use(express.urlencoded({
    extended: false
}));
app.use("/", landingpageRoute);
app.use("/game", gameRoute);
app.use("/formlogin", formloginRoute);
app.use("/login", loginRoute);
app.use("/api", apiRouter);
app.use("/formregister", formregisterRoute);
app.use("/users", usersRoute);

app.use((req, res, next) => {
    res.status(404).render("notfound");
});

app.listen(port, () => {
    console.log(`Server running on port ${port}`);
});