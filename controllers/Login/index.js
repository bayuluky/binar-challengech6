const express = require("express");
const router = express();
const database = require("../../db/user.json");

router.use(express.Router());
router.use(express.json());

router.post("/", (req, res) => {
    let request = req.body;
    let userData = database;

    const findUser = userData.some(
        (item) =>
            item.username === request.username &&
            item.password === request.password
    );

    if (findUser) {
        res.status(200).redirect("/users");
    } else {
        res.status(401).send("You're not authenticated");
    }
});

module.exports = router;
