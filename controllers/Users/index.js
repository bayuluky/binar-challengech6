const {
    Router
} = require("express");
const express = require("express");
const router = express.Router();
const {
    UserGames,
    UserGameBiodata
} = require('../../models');

router.post('/register', (req, res) => {
    UserGames.create({
        username: req.body.username,
        email: req.body.email,
        password: req.body.password,
    }).then(result => {
        console.log(result);
        UserGameBiodata.create({
            name: req.body.name,
            address: req.body.address,
            user_id: result.id
        }).then(biodata => {
            console.log(biodata);
        })
        console.log("Data has been inserted successfully")
        res.status(200).redirect("/users")
    });
})

router.get('/', (req, res) => {
    UserGames.findAll({
            include: UserGameBiodata,
        })
        .then((data) => {
            res.render('users', {
                data
            });
        })
        .catch((error) => {
            console.log('Something Wrong', error);
        });
});

router.get('/:id', (req, res) => {
    UserGames.findOne({
        where: {
            id: req.params.id
        },
        include: UserGameBiodata,
    }).then((user) => {
        res.render('update', {
            user
        });
    });
});

router.post("/update/:id", (req, res) => {
    UserGames.update({
            username: req.body.username,
            email: req.body.email,
            password: req.body.password,
        }, {
            where: {
                id: req.params.id
            }
        })
        .then(user => {
            UserGameBiodata.update({
                    name: req.body.name,
                    address: req.body.address
                }, {
                    where: {
                        user_id: req.params.id
                    }
                })
                .then(result => {
                    console.log(result)
                    console.log("Data has been updated successfully")
                    res.status(200).redirect("/users")
                })
        })
})

router.get('/delete/:id', (req, res) => {
    UserGames.destroy({
        where: {
            id: req.params.id
        }
    }).then(
        () => {
            console.log("Data has been deleted successfully")
            res.status(200).redirect("/users")
        }
    );
});


module.exports = router;