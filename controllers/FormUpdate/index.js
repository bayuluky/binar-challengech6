const express = require("express");
const router = express();
const { UserGames, UserGameBiodata } = require('../../models');

router.use(express.Router());
router.use(express.json());
router.use(express.urlencoded({ extended: true }));

router.get("/:id", (req, res, next) => {
  UserGames.findOne({
  where: { id: req.params.id },
    include: UserGameBiodata,
  }).then((user) => {
    res.render('update', { user });
  });
});

module.exports = router;
